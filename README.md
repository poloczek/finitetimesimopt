This repository holds supplementary material of the article *Comparing the finite-time performance of simulation-optimization algorithms* by Naijia (Anna) Dong, David J. Eckman, Matthias Poloczek, Xueqi Zhao, and Shane G. Henderson.
The paper is available in the subdirectory paper/ and on arXiv at http://arxiv.org/abs/1705.07825.  The slides of our presentation at the 2017 Winter Simulation Conference are in slides/

The MATLAB code of the algorithms and benchmarks from the experimental evaluation is collected in the folders algorithms/ and benchmarks/ respectively. The plots from the paper are in plots/.

For any questions and comments, please contact Shane G. Henderson (sgh9@cornell.edu).
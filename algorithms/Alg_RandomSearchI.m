%==========================================================================
%                The Random Search with Insight Algorithm
%==========================================================================
% DATE
%        Nov 2016
%
% AUTHOR
%        Anna Dong
%
%==========================================================================
%
% INPUT
%        problem
%              Problem function name
%        problemseed
%              Substream index (integer >=1)
%        solverseed
%              Input seed for Random Search (integer between 1 and 2147483646)
%        numBudget
%                number of budgets to record, >=3; the spacing between
%                adjacent budget points should be about the same
%        logOption
%              produce log if =1, not write a log if =0
%        logfilename
%              string, no need for .txt
%
%
% OUTPUT
%        Ancalls
%              An array (size = 'NumSoln' X 1) of budget expended
%        A
%              An array (size = 'NumSoln' X 'dim') of solutions
%              returned by solver
%        Afn
%              An array (size = 'NumSoln' X 1) of estimates of expected
%              objective function value
%        AFnVar
%              An array of variances corresponding to
%              the objective function at A
%              Equals NaN if solution is infeasible
%        AFnGrad
%              An array of gradient estimates at A; not reported
%        AFnGardCov
%              An array of gradient covariance matrices at A; not reported
%        Aconstraint
%              A vector of constraint function estimators; not applicable
%        AConstraintCov
%              An array of covariance matrices corresponding to the
%              constraint function at A; not applicable
%        AConstraintGrad
%              An array of constraint gradient estimators at A; not
%              applicable
%        AConstraintGradCov
%              An array of covariance matrices of constraint gradient
%              estimators at A; not applicable
%
%==========================================================================

%% Random Search
function [Ancalls, A, Afn, AFnVar, AFnGrad, AFnGradCov, ...
    Aconstraint, AConstraintCov, AConstraintGrad, ...
    AConstraintGradCov] = Alg_RandomSearchI(problem, problemseed, solverseed, ...
    numBudget, logOption, logfilename)
% [Ancalls, A, Afn, AFnVar] = Alg_RandomSearchI('FacilityLocation', 19, 30, 4, 0)
% [Ancalls, A, Afn, AFnVar] = Alg_RandomSearchI('FacilityLocation', 19, 30, 4, 1, 'del')

%% Unreported
AFnGrad = NaN;
AFnGradCov = NaN;
Aconstraint = NaN;
AConstraintCov = NaN;
AConstraintGrad = NaN;
AConstraintGradCov = NaN;

%%
r = 30; % Runlength time for each solution
probstructHandle=str2func(strcat(problem, 'Structure'));
[~, ~, ~, ~, ~, ~, ~, x0, budgetR, ~, ~] = probstructHandle(1, solverseed); % Based on stream in prob struc
probHandle = str2func(problem);
NumFinSoln = numBudget; % Number of solutions returned by solver
budget = round(linspace(budgetR(1),budgetR(2),numBudget));

dim = size(x0, 2); % Solution dimension
[minmax, ~, ~, ~, ~, ~, ~, ssolsM, ~, ~, ~] = probstructHandle(dim+1, solverseed);
if min(budget) < r*(dim+2) % Need to evaluate all initial solns in ssolsM
    fprintf('A budget is too small for a good quality run of Nelder-Mead.');
    return
end
numAttemptsV = floor(budget/r); % Number of maximum pts to attemt eval at a budget
numGenV = numAttemptsV(end)*1000; % Size of the pool to (randomly) pick pts to eval
% Generate Random Variables
s1 = RandStream.create('mrg32k3a', 'NumStreams', 1,'seed',solverseed);
RandStream.setGlobalStream(s1);
[~, ~, ~, ~, ~, ~, ~, ssolsM0, ~, ~, ~] = probstructHandle(numGenV, problemseed);
temp = randperm(numGenV,numAttemptsV(end))-(dim+1);
ssolsM2 = ssolsM0(temp,:);

Ancalls = (numAttemptsV*r);
% Initialize
A = zeros(NumFinSoln, dim);
Afn = zeros(NumFinSoln, 1);
AFnVar = zeros(NumFinSoln, 1);
if logOption == 1 % Option to produce a log file tracking conversion history
    logfname = strcat(logfilename,'.txt');
    logfid = fopen(logfname, 'w');
end

%% Start Solving
display(['Maximum Attempts = ',num2str(numAttemptsV(end)),'.'])
fn0V = zeros(dim+1,1);
fnVar0V = zeros(dim+1,1);
for i1 = 1:dim+1
    [fn, FnVar, ~, ~, ~, ~, ~, ~] = probHandle(ssolsM(i1,:),r,problemseed);
    fn0V(i1) = -minmax*fn; % Minimize fn
    fnVar0V(i1) = FnVar;
end
[bestFn,bestFnId] = min(fn0V);
bestX = ssolsM(bestFnId,:);
bestFnVar = fnVar0V(bestFnId);
numAdditionalAttempts = diff([dim+1, numAttemptsV]);
ssolsM2 = ssolsM0(temp,:);
N = numAttemptsV(end);
fn2V = zeros(N,1);
fnVar2V = zeros(N,1);
for i2 = 1:N
        [fn2, FnVar2, ~, ~, ~, ~, ~, ~] = probHandle(ssolsM2(i2,:),r,problemseed);
        fn2V(i2) = -minmax*fn2; % Minimize fn
        fnVar2V(i2) = FnVar2;
end
Bref = 1;
N = 0;
while Bref <= numBudget
    N = N + numAdditionalAttempts(Bref);
    if logOption == 1
        fprintf(logfid, '======== Budget #%d, new Attempts = %d ========\n',Bref,numAdditionalAttempts(Bref));
    end
    % Pick N more pts to record
    tempFnM = fn2V(1:N,:);
    [bestFn2,bestId2] = min(tempFnM);
    bestX2 = ssolsM2(bestId2,:);
    bestFnVar2 = fnVar2V(bestId2);
    if bestFn > bestFn2
        bestFn = bestFn2;
        bestX = bestX2;
        bestFnVar = bestFnVar2;   
    end
    % Record Values
    A(Bref,:) = bestX;
    Afn(Bref) = -minmax*bestFn;
    AFnVar(Bref) = bestFnVar;
    % Return current best solution after each iteration of algorithm
    if logOption == 1
        fprintf(logfid, 'Current lowest vertex at x = %.4f,\n', bestX);
        fprintf(logfid,'Current lowest objective function value = %.4f,\n\n', -minmax*bestFn);
    end
    Bref = Bref + 1;
    
end

end
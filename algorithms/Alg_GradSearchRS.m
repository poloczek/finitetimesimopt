
%==========================================================================
%     The Gradient Search Random Search Algorithm (a series of budgets)
%==========================================================================
% DATE
%        Nov 2016
%
% AUTHOR
%        Xueqi Zhao
% * Basing on The Gradient Search Algorithm by Anna Dong and Nellie Wu
%==========================================================================
%
% INPUT
%        x0
%              Matrix (size = 'NumStartingSol' X 'dim') of 'NumStartingSol'
%              initial solutions to the solver
%              For Nelder-Mead, NumStartingSol = 1
%              Each initial solution is of size dim X 1
%               (input ignored)
%        problem
%              Problem function name
%        problemseed
%              Substream index (integer >=1)
%        solverseed
%              Input seed for Nelder-Mead (integer between 1 and 2147483646)
%        budget
%              Vector of size NumSoln, where NumSoln is
%              the number of solutions returned by the solver
%              for example, if budget = [500 1000] then NumSoln
%              is 2 and the solver returns best available solutions after
%              every 500 calls to the oracle
%               (input ignored)
%        logfilename
%
%
% OUTPUT
%        Ancalls
%              An array (size = 'NumSoln' X 1) of budget expended
%        A
%              An array (size = 'NumSoln' X 'dim') of solutions
%              returned by solver
%        Afn
%              An array (size = 'NumSoln' X 1) of estimates of expected
%              objective function value
%        AFnVar
%              An array of variances corresponding to
%              the objective function at A
%              Equals NaN if solution is infeasible
%        AFnGrad
%              An array of gradient estimates at A; not reported
%        AFnGardCov
%              An array of gradient covariance matrices at A; not reported
%        Aconstraint
%              A vector of constraint function estimators; not applicable
%        AConstraintCov
%              An array of covariance matrices corresponding to the
%              constraint function at A; not applicable
%        AConstraintGrad
%              An array of constraint gradient estimators at A; not
%              applicable
%        AConstraintGradCov
%              An array of covariance matrices of constraint gradient
%              estimators at A; not applicable
%
%==========================================================================

%% Gradient Search Constrained
function [Ancalls, A, Afn, AFnVar, AFnGrad, AFnGradCov, ...
    Aconstraint, AConstraintCov, AConstraintGrad, AConstraintGradCov] = ...
    Alg_GradSearchRS(problem, problemseed, solverseed, numBudget, ...
    logOption, logfilename)
%[Ancalls, A, Afn, AFnVar] = Alg_GradSearchRS('CtsNews', 19, 30, 4, 0)
%[Ancalls, A, Afn, AFnVar] = Alg_GradSearchRS('CtsNews', 19, 30, 4, 1, 'gg')

% Unreported
AFnGrad = NaN;
AFnGradCov = NaN;
Aconstraint = NaN;
AConstraintCov = NaN;
AConstraintGrad = NaN;
AConstraintGradCov = NaN;

% parameter input
%steph = 0.1; % magic...originially 1
thres = 0.000005;
r = 30;
RestartThres = 0.0001;

problemstructhandle=str2func(strcat(problem, 'Structure'));
% replace inputs x0 and budget
[~, ~, ~, ~, ~, ~, ~, x0, ~, ~, ~] = problemstructhandle(1, solverseed);
dim = size(x0, 2);
[minmax, ~, ~, ~, VarBds, ~, ~, x0, budgetR, ~, ~] = problemstructhandle(dim+1, solverseed);
%scale steph
%steph = min(abs(x0(2,:)-x0(1,:)))/3;
steph = min(abs(x0(2,:)-x0(1,:)))/3;
x0 = x0(1,:); %get 1 starting soln
probHandle = str2func(problem);
budget = round(linspace(budgetR(1),budgetR(2),numBudget));

% initialize
A = zeros(numBudget,dim);
Afn = zeros(numBudget,1);
AFnVar = zeros(numBudget,1);
Ancalls = zeros(numBudget,1);
if logOption == 1 % Option to produce a log file tracking conversion history
    logfname = strcat(logfilename,'.txt');
    logfid = fopen(logfname, 'w');
    
    fprintf(logfid, 'INITIAL %f\n', x0);
    fprintf(logfid, 'STEPSIZE %f\n', steph);
    fprintf(logfid,'\n\n\n');
end
Bspent = 0; % Total budget spent
Bref = 1; % The budget currently referred to, = 1,...,numBudget
iterCount = 1;

x0current = x0;
x0best = x0;
graV = zeros(1,dim);

[fn0current, fn0varcurrent, ~, ~, ~, ~, ~, ~] = probHandle(x0current,r,problemseed);
fn0current = -minmax*fn0current;
Bspent = Bspent + r;
fn0best = fn0current;
fn0varbest = fn0varcurrent;

% Compute increment for finite difference
% check budget
while budget(Bref) - Bspent < 2*dim*r % budget required for one update
    Ancalls(Bref) = Bspent;
    A(Bref,:) = x0best;
    Afn(Bref) = -minmax*fn0best;
    AFnVar(Bref) = fn0varbest;
    Bref = Bref+1;
    if Bref>numBudget
        if logOption == 1
            fprintf(logfid, '======== ITERATION #%d (BudgetSpent %d) ========\n', iterCount,Bspent);
            fprintf(logfid,'Budget is too small to compute increment for finite difference\n');
            fprintf(logfid,'\n');
        end
        return
    end
end

for i = 1:dim
    steph1 = steph;
    steph2 = steph;
    
    x1 = x0current;
    x1(i)=x1(i)+steph1; % step forward
    if x1(i)>VarBds(i,2) % if ==, then same as backward diff
        x1(i)=VarBds(i,2);
        steph1 = abs(x1(i)-x0current(i)); % can remove abs()
    end
    [fn1, ~, ~, ~, ~, ~, ~, ~] =probHandle(x1,r,problemseed);
    fn1 = -minmax*fn1;
    
    x2 = x0current;
    x2(i)=x2(i)-steph2; % step backward
    if x2(i)<VarBds(i,1) % if ==, then same as forward diff
        x2(i)=VarBds(i,1);
        steph2 = abs(x0current(i)-x2(i)); % can remove abs()
    end
    [fn2, ~, ~, ~, ~, ~, ~, ~] =probHandle(x2,r,problemseed);
    fn2 = -minmax*fn2;
    
    graV(i) = (fn1-fn2)/(steph1+steph2);
end
Bspent = Bspent + 2*dim*r;
steph = abs(sqrt(fn0varcurrent)./(sqrt(2*r)*graV));

% Generate solns for RS
n = floor(budget(numBudget)/r);
[~, ~, ~, ~, ~, ~, ~, x0rand, ~, ~, ~] = problemstructhandle(n, solverseed);
num = 2; 

while Bspent <= budget(numBudget)
    % check budget
    while budget(Bref) - Bspent < (2*dim+1)*r % budget required for one update
        Ancalls(Bref) = Bspent;
        A(Bref,:) = x0best;
        Afn(Bref) = -minmax*fn0best;
        AFnVar(Bref) = fn0varbest;
        Bref = Bref+1;
        if Bref>numBudget
            if logOption == 1
                fprintf(logfid, '======== ITERATION #%d (BudgetSpent %d) ========\n', iterCount,Bspent);
                fprintf(logfid,'Not completed due to budget limit\n');
                fprintf(logfid,'\n');
            end
            return
        end
    end
    
    % approximate gradient
    for i = 1:dim
        steph1 = steph(i);
        steph2 = steph(i);
        
        x1 = x0current;
        x1(i)=x1(i)+steph1; % step forward
        if x1(i)>VarBds(i,2) % if ==, then same as backward diff
            x1(i)=VarBds(i,2);
            steph1 = abs(x1(i)-x0current(i)); % can remove abs()
        end
        [fn1, ~, ~, ~, ~, ~, ~, ~] =probHandle(x1,r,problemseed);
        fn1 = -minmax*fn1;

        x2 = x0current;
        x2(i)=x2(i)-steph2; % step backward
        if x2(i)<VarBds(i,1) % if ==, then same as forward diff
            x2(i)=VarBds(i,1);
            steph2 = abs(x0current(i)-x2(i)); % can remove abs()
        end        
        [fn2, ~, ~, ~, ~, ~, ~, ~] =probHandle(x2,r,problemseed);
        fn2 = -minmax*fn2;
        
        graV(i) = (fn1-fn2)/(steph1+steph2);
    end
    
    t=2; % initialize step size
    xG = x0current - t.*graV;
    xG = checkCons(VarBds,xG,x0current);
    [fnG, fnGvar, ~, ~, ~, ~, ~, ~] =probHandle(xG,r,problemseed);
    fnG = -minmax*fnG;
    Bspent = Bspent + (2*dim+1)*r;
    % update best soln so far
    if fnG < fn0best
        x0best = xG;
        fn0best = fnG;
        fn0varbest = fnGvar;
    end
        
    while fnG>=fn0current && t>=thres % backtracking line search
        t = 0.5*t;
        xG = x0current - t.*graV;
        xG = checkCons(VarBds,xG,x0current);
        % check budget
        while budget(Bref) - Bspent < r
            Ancalls(Bref) = Bspent;
            A(Bref,:) = x0best;
            Afn(Bref) = -minmax*fn0best;
            AFnVar(Bref) = fn0varbest;     
            Bref = Bref+1;
            if Bref>numBudget
                if logOption == 1 
                    fprintf(logfid, '======== ITERATION #%d (BudgetSpent %d) ========\n', iterCount,Bspent);
                    fprintf(logfid,'Not completed due to budget limit\n');
                    fprintf(logfid,'\n');
                end
                return
            end
        end
        [fnG, fnGvar, ~, ~, ~, ~, ~, ~] =probHandle(xG,r,problemseed);
        fnG = -minmax*fnG;
        Bspent = Bspent + r;
        % update best soln so far
        if fnG < fn0best
            x0best = xG;
            fn0best = fnG;
            fn0varbest = fnGvar;
        end
    end
    
    % if no significant improvement, then randomly jump to another point
    if (fn0current-fnG < RestartThres*(1+abs(fnG)))&&(norm(x0current-xG)<sqrt(RestartThres)*(1+norm(xG)))&&(norm(graV)<=nthroot(RestartThres,3)*(1+abs(fnG)))&&(norm(graV)<fn0varcurrent)
        if logOption == 1
            fprintf(logfid, '======== ITERATION #%d (BudgetSpent %d) ========\n', iterCount,Bspent);
            fprintf(logfid, 'fn0current = %.4f,\n', fn0current);
            fprintf(logfid, 'fnG = %.4f,\n', fnG);
            fprintf(logfid, 'No Significant Improvement\n');
        end
        x0current = x0rand(num,:);
        num = num+1;
        if logOption == 1
            fprintf(logfid, '======== RANDOM JUMP ========\n');
            fprintf(logfid, 'new x0current = %.4f,\n', x0current);
        end            
        while budget(Bref) - Bspent < r
            Ancalls(Bref) = Bspent;
            A(Bref,:) = x0best;
            Afn(Bref) = -minmax*fn0best;
            AFnVar(Bref) = fn0varbest;     
            Bref = Bref+1;           
            if Bref>numBudget
                if logOption == 1 
                    fprintf(logfid, 'jump failed due to budget limit\n');
                    fprintf(logfid,'\n');
                end            
                return
            end
        end
        [fn0current, fn0varcurrent, ~, ~, ~, ~, ~, ~] =probHandle(x0current,r,problemseed);
        fn0current = -minmax*fn0current;
        Bspent = Bspent + r;
        if logOption == 1
            fprintf(logfid, 'new fn0current = %.4f,\n', fn0current);
            fprintf(logfid,'\n');
        end        
        % update best soln so far
        if fn0current < fn0best
            x0best = x0current;
            fn0best = fn0current;
            fn0varbest = fn0varcurrent;
        end
    else
        x0current = xG;
        fn0current = fnG;
        if logOption == 1
            fprintf(logfid, '======== ITERATION #%d (BudgetSpent %d) ========\n', iterCount,Bspent);
            fprintf(logfid,'Current point at x = %.4f,\n', x0current);
            fprintf(logfid,'Current objective function value = %.4f,\n', fn0current);
            fprintf(logfid,'Current budget referred to = %d,\n', Bref);  
            fprintf(logfid,'Best point at x = %.4f,\n', x0best);
            fprintf(logfid,'Best objective function value = %.4f,\n', fn0best);
            fprintf(logfid,'\n');
        end
    end
    
    iterCount = iterCount + 1;
end 
    
if logOption == 1
    fprintf(logfid,'Total number of iterations: %d\n', iterCount);
    fprintf(logfid,'Total number of jumps: %d\n', num-1);
    fprintf(logfid,'\nCompleted iterations.\n');
end


%     function [x0new,fn0new,fn0varnew,fn0,fn0var,UseBudget] = centralDiff(x0old,steph,t,thres,dim,probHandle,r,problemseed,minmax,VarBds)
%         x0new = x0old;
%         UseBudget = 0;
%         graV = zeros(1,dim);
% 
%         [fn0, fn0var, ~, ~, ~, ~, ~, ~] =probHandle(x0old,r,problemseed);
%         fn0 = -minmax*fn0;
%         fn0new = fn0;
%         fn0varnew = fn0var;
%         UseBudget = UseBudget + r;
%         for i = 1:dim
%             steph1 = steph;
%             steph2 = steph;
%             
%             x1 = x0old;
%             x1(i)=x1(i)+steph1;
%             if x1(i)>VarBds(i,2) % if ==, then same as backward diff
%                 x1(i)=VarBds(i,2);
%                 steph1 = abs(x1(i)-x0old(i)); % can remove abs()
%             end
%             [fn1, ~, ~, ~, ~, ~, ~, ~] =probHandle(x1,r,problemseed);
%             fn1 = -minmax*fn1;
%             UseBudget = UseBudget + r;
%             
%             x2 = x0old;
%             x2(i)=x2(i)-steph2;
%             if x2(i)<VarBds(i,1) % if ==, then same as forward diff
%                 x2(i)=VarBds(i,1);
%                 steph2 = abs(x0old(i)-x2(i)); % can remove abs()
%             end
%             [fn2, ~, ~, ~, ~, ~, ~, ~] =probHandle(x2,r,problemseed);
%             fn2 = -minmax*fn2;
%             UseBudget = UseBudget + r;
%             
%             graV(i) = (fn1-fn2)/(steph1+steph2);
%         end
%         
%         xG = x0old - t.*graV;
%         xG = checkCons(VarBds,xG,x0old);
%         [fnG, fnGVar, ~, ~, ~, ~, ~, ~] =probHandle(xG,r,problemseed);
%         fnG = -minmax*fnG;
%         UseBudget = UseBudget + r;
%         
%         while fnG>=fn0 && t>=thres
%             t = 0.5*t;
%             xG = x0old - t.*graV;
%             xG = checkCons(VarBds,xG,x0old);
%             if budgetUpdate < r
%                 display('Run out of budget new x0!')
%                 return;
%             end
%             [fnG, fnGVar, ~, ~, ~, ~, ~, ~] =probHandle(xG,r,problemseed);
%             fnG = -minmax*fnG;
%             budgetUpdate = budgetUpdate - r;
%         end
%         if fnG<fn0
%             x0new = xG;
%             fn0new = fnG;
%             fn0varnew = fnGVar;
%         end
%     end


%% check and modify (if needed) the new point, based on VarBds.
    function modiSsolsV = checkCons(VarBds,ssolsV,ssolsV2) 
        col = size(ssolsV,2);
        stepV = ssolsV - ssolsV2;
        %t = 1; % t>0 for the correct direction
        tmaxV = ones(2,col);
        uV = VarBds(stepV>0,2); uV = uV';
        lV = VarBds(stepV<0,1); lV = lV';
        if length(uV)> 0
            tmaxV(1,stepV>0) = (uV - ssolsV2(stepV>0)) ./ stepV(stepV>0);
        end
        if length(lV)>0
            tmaxV(2,stepV<0) = (lV - ssolsV2(stepV<0)) ./ stepV(stepV<0);
        end
        t2 = min(min(tmaxV));
        modiSsolsV = ssolsV2 + t2*stepV;
        %rounding error
        for kc=1:col
            if modiSsolsV(kc)<0 && modiSsolsV(kc)>-0.00000005
                modiSsolsV(kc) = 0;
            end
        end
    end

end


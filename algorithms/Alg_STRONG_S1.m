
%==========================================================================
%               The STRONG Algorithm (a series of budget)
%==========================================================================
% DATE
%        Feb 2017
%
% AUTHOR
%        Xueqi Zhao and Naijia Dong
%
% * Basing on The STRONG Algorithm by by Kuo-Hao Chang 2008/4, but used
% finite diff instead of DOE
%==========================================================================
%
% INPUT
%        x0
%              Matrix (size = 'NumStartingSol' X 'dim') of 'NumStartingSol'
%              initial solutions to the solver
%              For Nelder-Mead, NumStartingSol = 1
%              Each initial solution is of size dim X 1
%               (input ignored)
%        problem
%              Problem function name
%        problemseed
%              Substream index (integer >=1)
%        solverseed
%              Input seed for Nelder-Mead (integer between 1 and 2147483646)
%        budget
%              Vector of size NumSoln, where NumSoln is
%              the number of solutions returned by the solver
%              for example, if budget = [500 1000] then NumSoln
%              is 2 and the solver returns best available solutions after
%              every 500 calls to the oracle
%               (input ignored)
%        logfilename
%
%
% OUTPUT
%        Ancalls
%              An array (size = 'NumSoln' X 1) of budget expended
%        A
%              An array (size = 'NumSoln' X 'dim') of solutions
%              returned by solver
%        Afn
%              An array (size = 'NumSoln' X 1) of estimates of expected
%              objective function value
%        AFnVar
%              An array of variances corresponding to
%              the objective function at A
%              Equals NaN if solution is infeasible
%        AFnGrad
%              An array of gradient estimates at A; not reported
%        AFnGardCov
%              An array of gradient covariance matrices at A; not reported
%        Aconstraint
%              A vector of constraint function estimators; not applicable
%        AConstraintCov
%              An array of covariance matrices corresponding to the
%              constraint function at A; not applicable
%        AConstraintGrad
%              An array of constraint gradient estimators at A; not
%              applicable
%        AConstraintGradCov
%              An array of covariance matrices of constraint gradient
%              estimators at A; not applicable
%
%==========================================================================

%% STRONG
function [Ancalls, A, Afn, AFnVar, AFnGrad, AFnGradCov, ...
    Aconstraint, AConstraintCov, AConstraintGrad, ...
    AConstraintGradCov] = Alg_STRONG_S1(problem, problemseed, solverseed, ...
    numBudget, logOption, logfilename)
% [Ancalls, A, Afn, AFnVar] = Alg_STRONG('EOQ', 19, 30, 4, 0)
% [Ancalls, A, Afn, AFnVar] = Alg_STRONG('EOQ', 19, 30, 4, 1, 'gg')

%% Unreported
AFnGrad = NaN;
AFnGradCov = NaN;
Aconstraint = NaN;
AConstraintCov = NaN;
AConstraintGrad = NaN;
AConstraintGradCov = NaN;

%%% Set default values.
r = 30; % Runlength time for each solution
problemstructhandle=str2func(strcat(problem, 'Structure'));
[~, ~, ~, ~, ~, ~, ~, x0, ~, ~, ~] = problemstructhandle(1, solverseed); % Based on stream in prob struc
dim = size(x0, 2); % Solution dimension
[minmax, ~, ~, ~, VarBds, ~, ~, x0, budgetR, ~, ~] = problemstructhandle(dim+1, solverseed);
solution = x0(1,:);
probHandle = str2func(problem);
budget = round(linspace(budgetR(1),budgetR(2),numBudget));
%%%

% initialize
A = zeros(numBudget,dim);
Afn = zeros(numBudget,1);
AFnVar = zeros(numBudget,1);
Ancalls = zeros(numBudget,1);
if logOption == 1 % Option to produce a log file tracking conversion history
    logfname = strcat(logfilename,'.txt');
    logfid = fopen(logfname, 'w');
    
    fprintf(logfid, 'INITIAL %f\n', solution);
    fprintf(logfid,'\n\n\n');
end
Bspent = 0; % Total budget spent
Bref = 1; % The budget currently referred to, = 1,...,numBudget
iterCount = 1;

%delta_threshold=1.2;
delta_T=2;      %the size of trust region
eta_0=0.01;     %the constant of accepting
eta_1=0.3;
gamma1=0.9;     %the constant of shrinking the trust regionthe new solution
gamma2=1.11;    %the constant of expanding the trust region

[Q_bar_old, fn0varcurrent, ~, ~, ~, ~, ~, ~] = probHandle(solution,r,problemseed);
Q_bar_old = -minmax*Q_bar_old;
Bspent = Bspent + r;
x0best = solution;
fn0best = Q_bar_old;
fn0varbest = fn0varcurrent;

%..........................Main Framework..............................
while Bspent <= budget(numBudget)
    % check variable bounds
    forward = (solution == VarBds(:,1)');
    backward = (solution == VarBds(:,2)');
    BdsCheck = forward - backward;
    % BdsCheck: 1 stands for forward, -1 stands for backward, 0 means central diff
    
    % Only stage I: local linear model
    if logOption == 1
        fprintf(logfid, '======== ITERATION #%d (BudgetSpent %d) ========\n', iterCount,Bspent);
        fprintf(logfid,'Current Solution %f\n',solution);
        fprintf(logfid,'function value %f\n',-minmax*Q_bar_old);
        fprintf(logfid,'\n');
    end
    % check budget
    NumOfEval = 2*dim - sum(BdsCheck~=0); % num of fn evaluations to compute grad
    while budget(Bref) - Bspent < (NumOfEval+1)*r % budget required for one update
        Ancalls(Bref) = Bspent;
        A(Bref,:) = x0best;
        Afn(Bref) = -minmax*fn0best;
        AFnVar(Bref) = fn0varbest;
        Bref = Bref+1;
        if Bref>numBudget
            if logOption == 1
                fprintf(logfid, '======== ITERATION #%d (BudgetSpent %d) ========\n', iterCount,Bspent);
                fprintf(logfid,'Not completed due to budget limit\n');
                fprintf(logfid,'\n');
            end
            return
        end
    end
    %step1 Build the linear model
    [Grad, Hessian]=FiniteDiff(solution,Q_bar_old,BdsCheck,r);
    Bspent = Bspent + NumOfEval*r;
    %step2 Solve the subproblem
    [new_solution]=Cauchy_point(Grad,Hessian,solution); %generate the new solution
    %step3 Compute the ratio
    %[Q_bar_old, ~, ~, ~, ~, ~, ~, ~] = probHandle(solution,N_center,problemseed);
    [Q_bar_new, newVar, ~, ~, ~, ~, ~, ~] = probHandle(new_solution,r,problemseed);
    Q_bar_new = -minmax*Q_bar_new;
    Bspent = Bspent + r;
    r_old = Q_bar_old;
    r_new = Q_bar_old+(new_solution-solution)*Grad+1/2*(new_solution-solution)*Hessian*(new_solution-solution)';
    rho=(Q_bar_old-Q_bar_new)/(r_old-r_new);
    %step4 Update the trust region size and determine to accept or reject the solution
    if rho < eta_0 || (Q_bar_old-Q_bar_new)<=0 || (r_old-r_new)<=0
        delta_T=gamma1*delta_T;
        %result_solution=solution;
        if logOption == 1
            fprintf(logfid, '======== ITERATION #%d (BudgetSpent %d) ========\n', iterCount,Bspent);
            fprintf(logfid,'New Solution %f\n',new_solution);
            fprintf(logfid,'function value %f\n',-minmax*Q_bar_new);
            fprintf(logfid,'Reject\n');
            fprintf(logfid,'\n');
        end
    elseif eta_0<=rho<eta_1
        solution=new_solution; %accept the solution and remains the size of  trust ra=[]egion
        Q_bar_old = Q_bar_new;
        if logOption == 1
            fprintf(logfid, '======== ITERATION #%d (BudgetSpent %d) ========\n', iterCount,Bspent);
            fprintf(logfid,'New Solution %f\n',new_solution);
            fprintf(logfid,'function value %f\n',-minmax*Q_bar_new);
            fprintf(logfid,'Accept\n');
            fprintf(logfid,'\n');
        end
        % update best soln so far
        if Q_bar_new < fn0best
            x0best = solution;
            fn0best = Q_bar_new;
            fn0varbest = newVar;
        end
    else
        delta_T=gamma2*delta_T;
        solution=new_solution;  %accept the solution and expand the size of trust reigon
        Q_bar_old = Q_bar_new;
        if logOption == 1
            fprintf(logfid, '======== ITERATION #%d (BudgetSpent %d) ========\n', iterCount,Bspent);
            fprintf(logfid,'New Solution %f\n',new_solution);
            fprintf(logfid,'function value %f\n',-minmax*Q_bar_new);
            fprintf(logfid,'Accept\n');
            fprintf(logfid,'\n');
        end
        % update best soln so far
        if Q_bar_new < fn0best
            x0best = solution;
            fn0best = Q_bar_new;
            fn0varbest = newVar;
        end
    end
    r=ceil(1.01*r);    
    iterCount = iterCount + 1;
end

%% Helper Function FiniteDiff
    function [Grad,Hessian]=FiniteDiff(solution,fn,BdsCheck,runlen)
        dim = size(solution, 2);
        FnPlusMinus = zeros(dim,3); % store values for each dimension:
        % col1: f(x+h,y)
        % col2: f(x-h,y)
        % col3: stepsize h
        Grad = zeros(dim,1);
        Hessian = zeros(dim,dim);
        
        for i = 1:dim
            % initialization
            x1 = solution;
            x2 = solution;
            steph1 = delta_T; %forward stepsize
            steph2 = delta_T; %backward stepsize
            
            % check VarBds
            if x1(i)+steph1 > VarBds(i,2)
                steph1 = abs(VarBds(i,2)-x1(i)); % can remove abs()
            end
            if x2(i)-steph2 < VarBds(i,1)
                steph2 = abs(x2(i)-VarBds(i,1)); % can remove abs()
            end
            
            % decide stepsize
            if BdsCheck(i) == 0    % central diff
                FnPlusMinus(i,3) = min(steph1, steph2);
                x1(i) = x1(i) + FnPlusMinus(i,3);
                x2(i) = x2(i) - FnPlusMinus(i,3);
            elseif BdsCheck(i) == 1    % forward diff
                FnPlusMinus(i,3) = steph1;
                x1(i) = x1(i) + FnPlusMinus(i,3);
            else    % backward diff
                FnPlusMinus(i,3) = steph2;
                x2(i) = x2(i) - FnPlusMinus(i,3);
            end
            
            if BdsCheck(i) ~= -1
                [fn1, ~, ~, ~, ~, ~, ~, ~] =probHandle(x1,runlen,problemseed);
                fn1 = -minmax*fn1;
                FnPlusMinus(i,1) = fn1; % first column is f(x+h,y)
            end
            
            if BdsCheck(i) ~= 1
                [fn2, ~, ~, ~, ~, ~, ~, ~] =probHandle(x2,runlen,problemseed);
                fn2 = -minmax*fn2;
                FnPlusMinus(i,2) = fn2; % second column is f(x-h,y)
            end
            
            if BdsCheck(i) == 0
                Grad(i) = (fn1-fn2)/(2*FnPlusMinus(i,3));
            elseif BdsCheck(i) == 1
                Grad(i) = (fn1-fn)/FnPlusMinus(i,3);
            elseif BdsCheck(i) == -1
                Grad(i) = (fn-fn2)/FnPlusMinus(i,3);
            end
        end
    end
%% Helper Function CauchyPoint
    function [Cauchy_point]=Cauchy_point(G,B,solution) %%Finding the Cauchy Point
        Q=G'*B*G;
        b=(-1)*delta_T/norm(G)*G';
        if Q <=0
            tau=1;
        else
            if (norm(G))^3/(delta_T*Q)<1
                tau=(norm(G))^3/(delta_T*Q);
            else
                tau=1;
            end
        end
        new_point=solution+tau*b;
        Cauchy_point = checkCons(new_point,solution);
    end
%% Helper Function CheckCons
    function modiSsolsV = checkCons(ssolsV,ssolsV2)
        col = size(ssolsV,2);
        stepV = ssolsV - ssolsV2;
        %t = 1; % t>0 for the correct direction
        tmaxV = ones(2,col);
        uV = VarBds(stepV>0,2); uV = uV';
        lV = VarBds(stepV<0,1); lV = lV';
        if length(uV)> 0
            tmaxV(1,stepV>0) = (uV - ssolsV2(stepV>0)) ./ stepV(stepV>0);
        end
        if length(lV)>0
            tmaxV(2,stepV<0) = (lV - ssolsV2(stepV<0)) ./ stepV(stepV<0);
        end
        t2 = min(min(tmaxV));
        modiSsolsV = ssolsV2 + t2*stepV;
        %rounding error
        for kc=1:col
            if modiSsolsV(kc)<0 && modiSsolsV(kc)>-0.00000005
                modiSsolsV(kc) = 0;
            end
        end
    end
end

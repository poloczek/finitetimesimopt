%==========================================================================
%                       The SPSA Algorithm
%==========================================================================
% DATE
%        Feb 2017
%
% AUTHOR
%        Anna Dong
% REFERENCE		Spall. An Overview of the Simultaneous Perturbation Method
%               for Efficient Optimization.
%==========================================================================
%
% INPUT
%        problem
%              Problem function name
%        problemseed
%              Substream index (integer >=1)
%        solverseed
%              Input seed for Nelder-Mead (integer between 1 and 2147483646)
%        numBudget
%                number of budgets to record, >=3; the spacing between
%                adjacent budget points should be about the same
%        logOption
%              produce log if =1, not write a log if =0
%        logfilename
%              string, no need for .txt
%
%
% OUTPUT
%        Ancalls
%              An array (size = 'NumSoln' X 1) of budget expended
%        A
%              An array (size = 'NumSoln' X 'dim') of solutions
%              returned by solver
%        Afn
%              An array (size = 'NumSoln' X 1) of estimates of expected
%              objective function value
%        AFnVar
%              An array of variances corresponding to
%              the objective function at A
%              Equals NaN if solution is infeasible
%        AFnGrad
%              An array of gradient estimates at A; not reported
%        AFnGardCov
%              An array of gradient covariance matrices at A; not reported
%        Aconstraint
%              A vector of constraint function estimators; not applicable
%        AConstraintCov
%              An array of covariance matrices corresponding to the
%              constraint function at A; not applicable
%        AConstraintGrad
%              An array of constraint gradient estimators at A; not
%              applicable
%        AConstraintGradCov
%              An array of covariance matrices of constraint gradient
%              estimators at A; not applicable
%
%==========================================================================

%% Simultaneous Perturbation Stochastic Approximation (SPSA)
function [Ancalls, A, Afn, AFnVar, AFnGrad, AFnGradCov, ...
    Aconstraint, AConstraintCov, AConstraintGrad, ...
    AConstraintGradCov] = Alg_SPSA(problem, problemseed, solverseed, ...
    numBudget, logOption, logfilename)
%% Unreported
AFnGrad = NaN;
AFnGradCov = NaN;
Aconstraint = NaN;
AConstraintCov = NaN;
AConstraintGrad = NaN;
AConstraintGradCov = NaN;

probstructHandle=str2func(strcat(problem, 'Structure'));
[minmax, ~, ~, ~, VarBds, ~, ~, x0, budgetR, ~, ~] = probstructHandle(1, solverseed); % Based on stream in prob struc
probHandle = str2func(problem);
NumFinSoln = numBudget; % Number of solutions returned by solver
budget = round(linspace(budgetR(1),budgetR(2),numBudget));
dim = size(x0, 2); % Solution dimension
if logOption == 1 % Option to produce a log file tracking conversion history
    logfname = strcat(logfilename,'.txt');
    logfid = fopen(logfname, 'w');
end

% Set default values.
r = 30;
nEvalSTD = 10;
if min(budget) < r*(nEvalSTD+3) % Check if budget enough
    fprintf('A budget is too small for a good quality run of SPSP.');
    return
end
[~, ~, ~, ~, ~, ~, ~, x0, ~, ~, ~] = probstructHandle(dim+1, solverseed);
theta0 = x0(1,:); % initial solution
alpha=0.602;
gamma = 0.101;
step= 1; % 'What is the initial desired magnitude of change in the theta elements?'
gavg= 1; % 'How many averaged SP gradients will be used per iteration? '
% Compute stdev of measurement noise
sobjfM = zeros(nEvalSTD,1);
svarV = zeros(nEvalSTD,1);
stdSeed = problemseed:problemseed + nEvalSTD - 1;
for kstd = 1:nEvalSTD
    [sobjfM(kstd),~] = probHandle(x0(end,:),r,stdSeed(kstd));
end
stdevf = std(sobjfM); % 'What is the standard deviation of the measurement noise at i.c.? '
c = max(stdevf/gavg^0.5, .0001); %averaging set up for independent noise
% Do the NL/(2*gavg) SP gradient estimates
NL = 2; % 'How many loss function evaluations do you want to use in this gain calculation? '
% Generate new streams for delta generation % noise in loss measurements
deltaGenStream = RandStream.create('mrg32k3a');
% Set the substream to the "solver seed"
% deltaGenStream.Substream = solverseed;
% OldStream = RandStream.setGlobalStream(deltaGenStream);

% Evaluate theta0
[ftheta, fthetaVar, ~, ~, ~, ~, ~, ~] = probHandle(theta0,r,problemseed);
A = ones(NumFinSoln,1)*theta0;
Afn = ones(NumFinSoln,1)*ftheta;
AFnVar = ones(NumFinSoln,1)*fthetaVar;
Ancalls = ones(NumFinSoln,1)*( (nEvalSTD+1) * r);
% A = zeros(NumFinSoln,dim);
% Afn = zeros(NumFinSoln,1);
% AFnVar = zeros(NumFinSoln,1);
% Ancalls = zeros(NumFinSoln,1);

display(['Maximum Budget = ',num2str(budgetR(2)),'.'])
% Loop. Each budget
for Bref = 1:NumFinSoln
    deltaGenStream.Substream = solverseed;
    OldStream = RandStream.setGlobalStream(deltaGenStream);
    RandStream.setGlobalStream(deltaGenStream);
    
    Bspent = (nEvalSTD + 1) * r;
    theta = theta0;
    budgetk = budget(Bref);
    nEvals = round( ((budgetk - Bspent)/r)/3 *2); % 'What is the expected number of loss evaluations per run? '
    Aalg = .10*nEvals/(2*gavg); 
    gbar=zeros(1,dim);
    %solsM = zeros(2, dim); % record solutions attempted
    %yV = zeros(2, 1); % record obj values of solutions attempted
    %yVar = zeros(2, 1); % record variance of solutions attempted
    for i=1:NL/(2*gavg)
       ghat=zeros(1,dim);
       for j=1:gavg
          delta=2*round(rand(1,dim))-1; % random delta
          %theta
          thetaplus=theta+c*delta;
          thetaminus=theta-c*delta;
          thetaplus2 = checkCons(VarBds,thetaplus,theta);
          thetaminus2 = checkCons(VarBds,thetaminus,theta);
          %solsM = [thetaplus2; thetaminus2];
          [fn, ~] = probHandle(thetaplus2,r,problemseed); 
          yplus=-minmax*fn;
          [fn, ~] = probHandle(thetaminus2,r,problemseed); 
          yminus=-minmax*fn; % enough for these 2
          %yV = [yplus; yminus];
          ghat=(yplus-yminus)./(2*c*delta)+ghat;
       end
       gbar=gbar+abs(ghat/gavg);
    end
    meangbar=mean(gbar);
    meangbar=meangbar/(NL/(2*gavg));
    a=step*((Aalg+1)^alpha)/meangbar;
    
    Bspent = Bspent + 2*r;
    k = 0;
    while Bspent <= budgetk -3*r % Check if finish referring to current Budget
        ak=a/(k+1+Aalg)^alpha;
        ck=c/(k+1)^gamma;
        delta=2*round(rand(1,dim))-1;
        thetaplus=theta+ck*delta;
        thetaminus=theta-ck*delta;
        thetaplus2 = checkCons(VarBds,thetaplus,theta);
        thetaminus2 = checkCons(VarBds,thetaminus,theta);
        %solsM = [thetaplus2; thetaminus2]
        [fn, ~] = probHandle(thetaplus2,r,problemseed); 
        yplus=-minmax*fn;
        [fn, ~] = probHandle(thetaminus2,r,problemseed); 
        yminus=-minmax*fn; % enough for these 2
        %yV = [yplus; yminus]
        ghat=(yplus-yminus)./(2*c*delta);
        theta2=theta-ak*ghat;
        
        % try 
        theta = checkCons(VarBds,theta2,theta);
        % evaluate f(theta)
        [ftheta, fthetaVar, ~, ~, ~, ~, ~, ~] = probHandle(theta,r,problemseed);
        Bspent = Bspent + 3*r; % update budget spent
        % Record current best soln
        if -minmax*ftheta < -minmax*Afn(Bref)
            A(Bref,:) = theta;
            Afn(Bref) = ftheta;
            AFnVar(Bref) = fthetaVar;
        end        
        Ancalls(Bref) = Bspent;
            % Return current best solution after each iteration of algorithm
        if logOption == 1
            fprintf(logfid, '======== Budget Spent = %d, @Budget %d ========\n', Bspent, Bref);
            fprintf(logfid, 'Current lowest vertex at x = %.4f,\n', A(Bref,:));
            fprintf(logfid,'Current lowest objective function value = %.4f,\n\n', Afn(Bref));
        end
        k = k + 1;
    end
    
    RandStream.setGlobalStream(OldStream);
end    

%% Helper Functions
% Helper 1: Check & Modify (if needed) the new point, based on VarBds.
% ssolsV2 original solution. ssolsV expected solution.
    function modiSsolsV = checkCons(VarBds,ssolsV,ssolsV2)
%         % Alternative
%         thetamax = VarBds(:,2)';
%         thetamin = VarBds(:,1)';
%         modiSsolsV = min(ssolsV, thetamax);
%         modiSsolsV = max(modiSsolsV, thetamin);
        %
        col = size(ssolsV,2);
        stepV = ssolsV - ssolsV2;
        % t>0 for the correct direction
        tmaxV = ones(2,col);
        uV = VarBds(stepV>0,2); uV = uV';
        lV = VarBds(stepV<0,1); lV = lV';
        if ~isempty(uV) %length(uV)> 0
            tmaxV(1,stepV>0) = (uV - ssolsV2(stepV>0)) ./ stepV(stepV>0);
        end
        if ~isempty(lV) %length(lV)>0
            tmaxV(2,stepV<0) = (lV - ssolsV2(stepV<0)) ./ stepV(stepV<0);
        end
        t = min(min(tmaxV));
        modiSsolsV = ssolsV2 + t*stepV;
        %rounding error, may remove
        for kc=1:col
            if abs(modiSsolsV(kc))<0.00000005
                modiSsolsV(kc) = 0;
            end
        end
    end


end



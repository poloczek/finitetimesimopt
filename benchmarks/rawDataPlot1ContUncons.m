function rawDataPlot1ContUncons(problemname)
% By Anna Dong
% Plot on continuous, unconstrained problems.

AlgsNames = {'Alg_RandomSearchI','Alg_NelderMead','Alg_GradSearchRS','Alg_SPSA','Alg_STRONG'};
AlgsNamesLegend = {'Random Search','Nelder-Mead','Gradient Search RS','SPSA','STRONG'};
%AlgsNames = {'Alg_NelderMead','Alg_GradSearchRS','Alg_STRONG'};
%AlgsNamesLegend = {'NelderMead','GradSearchRS','STRONG'};
%AlgsNames = {'Alg_RandomSearchI','Alg_NelderMead','Alg_GradSearch','Alg_GradSearchRS','Alg_GradSearchLS'};
% %Testing, to be removed
%problemname = 'SAN';
%plotseed = 99;
AlgsColors = {'m','b','g','r','c'};
minmaxList = {'minimize','nah','maximize'};
numBudget = 50;

%plot 1. How different algorithms perform as budget varies (increases) on a
%problem
problemstructhandle=str2func(strcat(problemname, 'Structure'));
[minmax, ~, ~, ~, ~, ~, ~, ~, budgetR, ~, ~] = problemstructhandle(1, 19);

numAlgs = length(AlgsNames);
fnmean = zeros(numAlgs,numBudget+1);
EWidth = zeros(numAlgs,numBudget+1);
for j=1:numAlgs
    fnmean(j,:) = csvread(strcat(pwd,'/___RawData5/RawData_',AlgsNames{j},'_on_',problemname,'_mean.csv'));
    EWidth(j,:) = csvread(strcat(pwd,'/___RawData5/RawData_',AlgsNames{j},'_on_',problemname,'_errorWidth.csv'));
    %fnmean(j,:) = log(fnmean(j,:));
    %EWidth(j,:) = log(EWidth(j,:));
end

%rng(plotseed);
budget = round(linspace(budgetR(1),budgetR(2),numBudget));
beta = 0; % to avoid overlap
% (budget(2)-budget(1))/numAlgs 
increm = 10;
figure;
for i=1:numAlgs
    errorbar([0,budget+beta],fnmean(i,:),EWidth(i,:),'-o', 'Color', AlgsColors{i});
    hold on
    beta = beta + increm;
end
legend(AlgsNamesLegend);
xlabel('Budget','FontSize',12); 
ylabel('Objective value','FontSize',12);
title([minmaxList{minmax+2},' Problem: ',problemname],'FontSize',12);
miny = min(min(fnmean-EWidth));
maxy = max(max(fnmean+EWidth));
axis([0,max(budget)*1.01, miny, maxy]);


end
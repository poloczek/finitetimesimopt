%   ****************************************
%   *** Code written by German Gutierrez ***
%   ***         gg92@cornell.edu         ***
%   ***                                  ***
%   *** Updated by Shane Henderson to    ***
%   *** use standard calling and random  ***
%   *** number streams                   ***
%   *** Updated by Jennifer Shih         ***
%   *** July 1, 2014                     ***
%   *** Updated by Bryan Chong           ***
%   *** September 17, 2014               ***
%   ****************************************
%

% Last updated September 17, 2014

% RETURNS:   Average holding cost, Average penalty cost and Average ordering
%           cost per period.

function [fn, FnVar, FnGrad, FnGradCov, constraint, ConstraintCov, ConstraintGrad, ConstraintGradCov] = DualSourcing(x, runlength, seed, ~);
%function [fn, FnVar, FnGrad, FnGradCov, constraint, ConstraintCov, ConstraintGrad, ConstraintGradCov] = DualSourcing(x, runlength, seed, ~);
% x is the target levels to order
% runlength is the number of days of demand to simulate
% seed is the index of the substreams to use (integer >= 1)
% other is not used

constraint = NaN;
ConstraintCov = NaN;
ConstraintGrad = NaN;
ConstraintGradCov = NaN;

if ((sum(x < 0))>0) || (runlength <= 0) || (runlength ~= round(runlength)) || (seed <= 0) || (round(seed) ~= seed),
    fprintf('x should be >= 0, runlength should be positive integer, seed must be a positive integer\n');
    fn = NaN;
    FnVar = NaN;
    FnGrad = NaN;
    FnGradCov = NaN;
else
    
    cr=100; ce=110;                     %cost regular and expedited
    meanDemand=30; stdDemand=10;        %mean and Std. dev of demand (normal)
    le=0; lr=2; l=lr-le;                %lead time expedited and regular
    h=5;                                %holding cost
    pc=495;                             %penalty cost
    
    nDays=runlength;                    %LENGHT OF SIMULATION
    initialInv=40;                      %INITIAL INVENTORY
    ze=x(1); zr=x(2);                   %ORDER-UP TO LEVELS
    %Starting: ze=50, zr=80
    
    Xe=zeros(le,1); Xr=zeros(lr,1);     %vectors of orders to be received in
    %periods n through n+lr-1 for regular
    % and n+le-1 for expedited.
    
    % Generate a new stream for random numbers
    OurStream = RandStream.create('mrg32k3a');
    
    % Set the substream to the "seed"
    OurStream.Substream = seed;
    
    % Generate demands
    OldStream = RandStream.setGlobalStream(OurStream);
    pd = makedist('Normal', 'mu', meanDemand, 'sigma', stdDemand);
    truncatedPd = truncate(pd,0,inf);
    demand = random(truncatedPd, nDays, 1);
    RandStream.setGlobalStream(OldStream); % Restore previous stream
    
    %Track total expenses
    TotalHoldingCost=zeros(nDays,1); 
    TotalPenaltyCost=zeros(nDays,1);
    TotalOrderingCost=zeros(nDays,1);
    
    %Set inventory at period 1.
    In=initialInv;
    
    %set nDays to runlength
    nDays=runlength;
    
    %Go day by day updating inventory positions, levels and placing orders.
    for i=1:nDays
        %Calculate inventory positions
        IPen=In+sum(Xe)+sum(Xr(1:le,:));
        IPrn=In+sum(Xe)+sum(Xr);
        
        %Place orders as needed. Note that Xr,n-l enters IPen in next period,
        %thus it is contained while calculating Xe. Lastly, keep track of total
        %ordering cost.
        Xe=cat(1,Xe, max(0,ze-IPen-Xr(le+1,1)));
        Xr=cat(1,Xr,zr-IPrn-Xe(le+1,1));
        TotalOrderingCost(i)=ce*Xe(le+1,1)+cr*Xr(lr+1,1);
        
        %Orders Arrive, update on-hand inventory
        In=In+Xe(1,1)+Xr(1,1);
        Xe(1,:)=[]; Xr(1,:)=[];
        
        %Demand revealed
        dn=max(0,demand(i));
        
        %Satisfy or backorder Demand. Calculate expenses due to penalty costs
        %and holding costs.
        In=In-dn;
        TotalPenaltyCost(i)=-pc*min(0,In);
        TotalHoldingCost(i)=h*max(0,In);
    end
    fn = mean(TotalPenaltyCost) + mean(TotalHoldingCost) + mean(TotalOrderingCost);
    %fn = [mean(TotalPenaltyCost); mean(TotalHoldingCost); mean(TotalOrderingCost)];
    FnVar = (1/nDays)*[var(TotalPenaltyCost + TotalHoldingCost + TotalOrderingCost)];
    %FnVar = (1/nDays)*[var(TotalPenaltyCost); var(TotalHoldingCost); var(TotalOrderingCost)];
    FnGrad = NaN;
    FnGradCov = NaN;
end
end


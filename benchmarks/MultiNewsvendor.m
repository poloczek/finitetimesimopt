function [fn, FnVar, FnGrad, FnGradCov, constraint, ConstraintCov, ConstraintGrad, ConstraintGradCov] = MultiNewsvendor(x, runlength, seed, ~)
% x is a row vector of the number of units of each resource to stock
% runlength is the number of replications of to simulate
% seed is the index of the substreams to use (integer >= 1)
% other is not used
% Returns expected maximal operating profit

%   *************************************************************
%   ***                Written by Bryan Chong                 ***
%   ***       bhc34@cornell.edu    November 24th, 2012        ***
%   *************************************************************

constraint = NaN;
ConstraintCov = NaN;
ConstraintGrad = NaN;
ConstraintGradCov = NaN;
FnGrad=NaN;
FnGradCov=NaN;

integral = 0; % Set to 1 for integral production quantities, 0 for continuous.

if (sum(x < 0)>0) || (integral == 1 && (sum(x ~= round(x)) > 0)) || (runlength <= 0) || (runlength ~= round(runlength)) || (seed <= 0) || (round(seed) ~= seed),
    fprintf('x components must be nonnegative integers, runlength and seed must be a positive integers\n');
    fn = NaN;
    FnVar = NaN;
    FnGrad = NaN;
    FnGradCov = NaN;
else
    K = x;
    % Setting 1
    p = [9, 8, 7];  %price vector
    c = [5, 5, 5];  %cost vector
    n = length(c);  %number of products
    
    % Setup for LP/IP
    f = -(p-c); % minimize this quantity
    A = [1, 0, 0; 0, 1, 2]; % Aij = amt of resource i needed for product j
    b = K;                  %resource vector (decision variables)
    lb = zeros(n,1);
    
    % Setting 2
    %p = 30;
    %q = 30;
    alpha = 2;
    beta = 20;
    
    % Generate random demand
    DemandStream = RandStream.create('mrg32k3a');   % Generate a new stream for random numbers
    DemandStream.Substream = seed;  % Set the substream to the "seed"
    OldStream = RandStream.setGlobalStream(DemandStream);   % Temporarily store previous stream
    U = rand(n, runlength);
    
    % Inverse Transform Sampling
    % F(x) = 1 - (1+x^alpha)^(-beta)
    % F^-1(x) = ((1-x)^(-1/beta)-1)^(1/alpha)
    D = ((1-U).^(-1/beta)-1).^(1/alpha);
    RandStream.setGlobalStream(OldStream);   % Restore previous stream
    
    Pi = zeros(runlength,1);
    parfor k = 1:runlength
        ub = D(:, k);
        if integral == 0;
            [~, fval] = linprog(f, A, b, [], [], lb, ub);
        else
            [~, fval] = intlinprog(f, 1:n, A, b, [], [], lb, ub);
        end
        Pi(k) = -fval;
    end
    fn = mean(Pi);
    FnVar = var(Pi)/runlength;
end

function [fn, FnVar, FnGrad, FnGradCov, constraint, ConstraintCov, ConstraintGrad, ConstraintGradCov] = RoutePrices(p, runlength, seed, ~)

% p is vector of route prices
% runlength is the number of trials to simulate
% seed is the index of the substreams to use (integer >= 1)
% other is not used

% RETURNS: fn (total revenue from trips taken), FnVar

%   ********************************************
%   *** Code written by David Eckman         ***
%   ***        dje88@cornell.edu             ***
%   ********************************************

% Note: RandStream.setGlobalStream(stream) can only be used for Matlab
% versions 2011 and later
% For earlier versions, use the method RandStream.setDefaultStream(stream)

FnGrad = NaN;
FnGradCov = NaN;
constraint = NaN;
ConstraintCov = NaN;
ConstraintGrad = NaN;
ConstraintGradCov = NaN;

% Restrict p to lie in the nonnegative orthant
if ((sum(p < 0) >= 1) || (runlength <= 0) || (runlength ~= round(runlength)) || (seed <= 0) || (round(seed) ~= seed))
    fprintf('prices must be nonnegative, runlength should be positive integer, seed must be a positive integer\n');
    fn = NaN;
    FnVar = NaN;

else

    % Generate new streams for arrival process and travel times
    [PoissonProcessStream, TriangleStream, InitialStream] = RandStream.create('mrg32k3a', 'NumStreams', 3);

    % Set the substream to the "seed"
    PoissonProcessStream.Substream = seed;
    TriangleStream.Substream = seed;
    InitialStream.Substream = seed;
    
    % Temporarily store old stream
    OldStream = RandStream.setGlobalStream(InitialStream);
    %OldStream = RandStream.setDefaultStream(InitialStream); %versions 2010 and earlier

    % A, lambda, TriA, TriB, and TriC are problem-specific

	% Adjacency matrix for network
    A = [0, 1, 1, 1; 1, 0, 1, 1; 1, 1, 0, 1; 1, 1, 1, 0]; % A = ones(nStations,nStations) - eye(nStations) % fully connected network
    nStations = size(A,1); % Number of stations
    nRoutes = nnz(A); % Number of routes
    routes = find(A); % Indexes of routes in A matrix

    % Raw arrival rates (by route)
    lambda = [0, 3, 4, 5; 3, 0, 5, 6; 4, 5, 0, 7; 5, 6, 7, 0];
    
    % Route prices (as a matrix)
    route_p = zeros(nStations, nStations);
    route_p(routes) = p;
    route_p = route_p';

    % Effective arrival rates
    effLambda = lambda.*exp(-route_p); 

    % Make travel times triangular (a=0, b=1, c);
    TriA = zeros(nStations,nStations);
    TriB = ones(nStations,nStations);
    TriC = [0, 3/8, 4/8, 5/8; 3/8, 0, 5/8, 6/8; 4/8, 5/8, 0, 7/8; 5/8, 6/8, 7/8, 0];
    %TriC = [0, 0.25, 0.75, 0.5; 0.5, 0, 0.5, 0.25; 0.75, 0.75, 0, 0.5; 0.75, 0.5, 0.75, 0];
    
    T = 96; % Length of time horizon for sample path
    nCars = 10; % Number of cars in network (fixed)

	% Revenues for each simulation of the network
	revenue = zeros(runlength,1);

    for r = 1:runlength
        
        % Initialize system state
        NextEventTime = zeros(1, nCars);
        NextEventType = zeros(1, nCars);
        CurrentStation = zeros(1, nCars);
        NextStation = zeros(1, nCars);
        QueuePosition = zeros(1, nCars);

        % Randomly locate cars at stations and queues
        NextEventType = 2*ones(1, nCars); % 1 if arrival, 2 if departure
        CurrentStation = unidrnd(nStations, 1, nCars);

        % Set stream for generating passenger arrivals
        RandStream.setGlobalStream(PoissonProcessStream);
        %RandStream.setDefaultStream(PoissonProcessStream);
        
        for k = 1:nCars
            % Assign queue positions
            QueuePosition(k) = sum(CurrentStation(1:k) == CurrentStation(k));
            
            % Generate departure times and next destinations
            if QueuePosition(k) == 1;
                [NextEventTime(k), NextStation(k)] = min(exprnd(1./effLambda(CurrentStation(k),:)));
            else 
                [~, CarAheadInQueue] = max((CurrentStation == CurrentStation(k)).*(QueuePosition == QueuePosition(k) - 1));
                [NextEventTime(k), NextStation(k)] = min(NextEventTime(CarAheadInQueue) + exprnd(1./effLambda(CurrentStation(k),:)));
            end
        end

        % Set system clock time
        t = 0;

	    while t <= T
    
            % Advance system clock and identify the next event/car
            [t, CarToChange] = min(NextEventTime);

            if NextEventType(CarToChange) == 1 % If the event is an arrival...

                CurrentStation(CarToChange) = NextStation(CarToChange); % Update present location of car
                QueuePosition(CarToChange) = sum(QueuePosition == CurrentStation(CarToChange)); % Place in queue

                % Set stream for generating passenger arrivals
                RandStream.setGlobalStream(PoissonProcessStream);
                %RandStream.setDefaultStream(PoissonProcessStream);

                % Generate departure times and next destinations
                if QueuePosition(CarToChange) == 1;
                    [NextEventTime(CarToChange), NextStation(CarToChange)] = min(t + exprnd(1./effLambda(CurrentStation(CarToChange),:)));
                else 
                    [~, CarAheadInQueue] = max((CurrentStation == CurrentStation(CarToChange)).*(QueuePosition == QueuePosition(CarToChange) - 1));
                    [NextEventTime(CarToChange), NextStation(CarToChange)] = min(NextEventTime(CarAheadInQueue) + exprnd(1./effLambda(CurrentStation(CarToChange),:)));
                end

                % Next event for this car will be a departure
                NextEventType(CarToChange) = 2;


            else % If the event is a departure...

                % Collect payment at the start of the trip;
                revenue(r) = revenue(r) + route_p(CurrentStation(CarToChange), NextStation(CarToChange));

                % Set stream for generating travel times
                RandStream.setGlobalStream(TriangleStream);
                %RandStream.setDefaultStream(TriangleStream);

                % Generate travel time
                U = rand();
                O = CurrentStation(CarToChange); % Origin
                D = NextStation(CarToChange); % Destination
                if U < (TriC(O,D) - TriA(O,D))/(TriB(O,D) - TriA(O,D));
                    TravelTime = TriA(O,D) + sqrt(U*(TriB(O,D) - TriA(O,D))*(TriC(O,D) - TriA(O,D)));
                else
                    TravelTime = TriB(O,D) - sqrt((1-U)*(TriB(O,D) - TriA(O,D))*(TriB(O,D) - TriC(O,D)));
                end

                % Determine next event time and type for this car
                NextEventTime(CarToChange) = t + TravelTime; % Update next event time
                NextEventType(CarToChange) = 1; % Next event for this car will be an arrival
                            
                % Advance queue positions of cars at station that the car departed
                QueuePosition = QueuePosition - (CurrentStation == CurrentStation(CarToChange));
                % QueuePosition(CarToChange) becomes 0

                CurrentStation(CarToChange) = 0; % Mark as "en route"
                % NextStation(CarToChange) is unchanged
            end
	    end
    end

    %Restore old Random number stream
    RandStream.setGlobalStream(OldStream);
    %RandStream.setDefaultStream(OldStream); %versions 2010 and earlier

    % Record final mean and variance of cumulative costs
	fn = mean(revenue);
	FnVar = var(revenue);

end
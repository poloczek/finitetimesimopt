function [fn, FnVar, FnGrad, FnGradCov, constraint, ConstraintCov, ConstraintGrad, ConstraintGradCov] = MaintenanceOptimization(x, runlength, seed, ~)
% x is a row vector of the times of each Preventive Maintenance
% runlength is the number of replications of to simulate
% seed is the index of the substreams to use (integer >= 1)
% other is not used
% Returns expected maximal operating profit

%   *************************************************************
%   ***                Written by Bryan Chong                 ***
%   ***       bhc34@cornell.edu    January 12th, 2015         ***
%   *************************************************************

constraint = NaN;
ConstraintCov = NaN;
ConstraintGrad = NaN;
ConstraintGradCov = NaN;
FnGrad=NaN;
FnGradCov=NaN;

L = 10; % Length of simulation

if (sum(x < 0)>0) || (sum(x > L) > 0) || (runlength <= 0) || (runlength ~= round(runlength)) || (seed <= 0) || (round(seed) ~= seed),
    fprintf('x components must be nonnegative, runlength and seed must be a positive integers\n');
    fn = NaN;
    FnVar = NaN;
    FnGrad = NaN;
    FnGradCov = NaN;
else
    %% Parameters
    Cpm = 18998; %preventive manitenance cost
    Ccm = 97997; %corrective maintenance cost
    Ctrip = 4349781; %cost given trip occurs
    pTrip = 0.0073; %probability larger failure (trip) occurs)
    
    %% From Inputs/Parameters
    PMtimes = unique(x); % time of Preventive Maintenances (PM's) with duplicates removed
    numPMs = length(PMtimes);
    nReps = runlength; % Number of replications of simulations of length L
    interPMtimes = [PMtimes,L] - [0, PMtimes]; % the stretches of continuously un-maintained time
    numStretches = length(interPMtimes);
    
    %% Generate Random Variables
    [unifStream, expStream, pTripStream] = RandStream.create('mrg32k3a', 'NumStreams', 3);
    % Set the substream to the "seed"
    unifStream.Substream = seed;
    expStream.Substream = seed;
    pTripStream.Substream = seed;
    
    % Generate uniform random variables for acceptance-rejection method
    OldStream = RandStream.setGlobalStream(unifStream); % Temporarily store old stream
    UniVars = rand(nReps, 10000); % change later
    
    % Generate exponential random variables for acceptance-rejection method
    RandStream.setGlobalStream(expStream);
    ExpVars = exprnd(1,nReps, 10000); % change later
    
    % Generate probability of a "trip" during a failure
    RandStream.setGlobalStream(pTripStream);
    pTripVars = rand(nReps, 10000); % change later
    
    % Restore old random number stream
    RandStream.setGlobalStream(OldStream);
    
    %% Main Simulation

    % Find constant for Acceptance-Rejection (AR) method (See math)
    phi = 1/2 + sqrt(5)/2;
    const = fOg(phi,0);
    totCost = zeros(nReps,1);
    for rep = 1:nReps
        % Counters for the random variables
        UniCount = 1;
        ExpCount = 1;
        pTripCount = 1;
        cost = 0; % total cost
        
        % Each stretch of unmaintained time can be looked at independently
        for s = 1:numStretches
            c = 0; % current time (in this stretch)
            % Generate first failure from t*e^(-t^2/2) distribution via AR
            found = 0; % flag to stop AR method
            while found == 0
                e = ExpVars(rep, ExpCount); ExpCount = ExpCount + 1;
                u = UniVars(rep, UniCount); UniCount = UniCount + 1;
                if u < fOg(e,0)/const
                    c = c + e;
                    found = 1;
                end
            end
            
            % If previous failure is before maintenance, determine cost of
            % failure, then generate another time
            while c < interPMtimes(s)
                p = pTripVars(rep, pTripCount); pTripCount = pTripCount + 1;
                if p < pTrip
                    cost = cost + Ctrip;
                else
                    cost = cost + Ccm;
                end
                
                % Generate distribution from (t+c)*e^(-t^2/2-ct)
                % Find 'const' for this distribution. (See math notes)
                if c < phi
                    const2 = fOg((1-2*c+sqrt(5))/2, c);
                else
                    const2 = c;
                end
                found = 0;
                while found == 0
                    e = ExpVars(rep, ExpCount); ExpCount = ExpCount + 1;
                    u = UniVars(rep, UniCount); UniCount = UniCount + 1;
                    if u < fOg(e,c)/const2
                        c = c + e;
                        found = 1;
                    end
                end
            end
        end
        totCost(rep) = cost;
    end
    fn = numPMs*Cpm + mean(totCost);
    FnVar = var(totCost)/nReps;
end
end

% Calculate f over g for Acceptance-Rejection method
function [fOverg] = fOg(x, k)
fOverg = (x+k)*exp(-x^2/2-k*x+x);
end

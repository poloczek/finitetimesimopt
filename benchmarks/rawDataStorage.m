function rawDataStorage(problemname, solvername)
% Output: 3 vectors, each of dimension (1+numBudget), 1 being InitialSoln z
% By Anna Dong
% Nov, 2016 (Last Updated on March 2017)

% rawDataStorage('CtsNews', 'Alg_NelderMead')
problemseed = 19;
solverseed = 30;
numBudget = 50; % Number of budget to record, >=3

% Input
reps = 30; % Runlength for a problem
repsAlg = 30; % Replications of algorithms
CILevel = 0.95;
s1 = RandStream.create('mrg32k3a', 'NumStreams', 1,'seed',round(problemseed/2+solverseed/2));
RandStream.setGlobalStream(s1);
% Get problemhandle, solverhandle
probstructHandle=str2func(strcat(problemname, 'Structure'));
probHandle = str2func(problemname);
[~, dim, ~, ~, ~, ~, ~, ~, ~, ~, ~] = probstructHandle(1, solverseed);
solverHandle=str2func(solvername);
% Generate distinct prob seeds
seedpV=unidrnd(repsAlg*1000,repsAlg,1);
seedsV=unidrnd(repsAlg*1000,repsAlg,1);
seed1V=unidrnd(repsAlg*numBudget*1000,repsAlg,numBudget); 
seedInitV=unidrnd(repsAlg*1000,repsAlg,1); 
FMatrix = zeros(repsAlg,numBudget);
SMatrix = zeros(repsAlg,numBudget+1,dim);
for j=1:repsAlg  % Do 30 replications for each algo on the problem
    [~, soln, ~, ~, ~, ~, ~, ~, ~, ~] = solverHandle(problemname,seedpV(j),seedsV(j),numBudget,0);
    for k=1:numBudget
        seed1=seed1V(j,k);
        [fn, ~, ~, ~, ~, ~, ~, ~] = probHandle(soln(k,:), reps, seed1);
        FMatrix(j,k) = fn;
    end
    SMatrix(j,2:end,:) = soln;
end

% Store Initial Solution & Objective fcn values
FInitV = zeros(repsAlg,1);
for j2=1:repsAlg  % Do 30 replications for each algo on the problem
    [~, ~, ~, ~, ~, ~, ~, ssolsM, ~, ~, ~] = probstructHandle(dim+1, seedsV(j2));
    soln = ssolsM(1,:);
    [fn, ~, ~, ~, ~, ~, ~, ~] = probHandle(soln, reps, seedInitV(j2));
    FInitV(j2) = fn;
    SMatrix(j2,1,:) = soln;
end

% Clean & Store Data
FMatrix = [FInitV, FMatrix];
FVector = mean(FMatrix);
if repsAlg==1
    FVarVector = 0;
else
    FVarVector = var(FMatrix);
end
FnSEM = sqrt(FVarVector/repsAlg);
EWidth = norminv(1-(1-CILevel)/2,0,1)*FnSEM;

% Store data, wrt each budget
csvwrite(strcat(pwd,'/___RawData5/RawData_',solvername,'_on_',problemname,'_mean.csv'), FVector);
csvwrite(strcat(pwd,'/___RawData5/RawData_',solvername,'_on_',problemname,'_var.csv'), FVarVector);
csvwrite(strcat(pwd,'/___RawData5/RawData_',solvername,'_on_',problemname,'_errorWidth.csv'), EWidth);

csvwrite(strcat(pwd,'/___RawData5/RawData_',solvername,'_on_',problemname,'_30IterationsFn.csv'), FMatrix);
save(strcat(pwd,'/___RawData5/RawData_',solvername,'_on_',problemname,'_30IterationsSoln.mat'), 'SMatrix');


end


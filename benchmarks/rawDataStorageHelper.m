function rawDataStorageHelper(problemnameArray, solvernameArray)
% By Anna Dong
% Nov, 2016

% rawDataStorageHelper({'Rosenbrock'}, {'Alg_NelderMead','Alg_RandomSearchI','Alg_GradSearchRS','Alg_SPSA','Alg_STRONG'})
for k1 = 1:length(problemnameArray)
    for k2 = 1:length(solvernameArray)
        rawDataStorage(problemnameArray{k1}, solvernameArray{k2});
    end
end
        
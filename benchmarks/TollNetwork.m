function [fn, FnVar, FnGrad, FnGradCov, constraint, ConstraintCov, ConstraintGrad, ConstraintGradCov] = TollNetwork(x, runlength, seed, ~)

% x is vector of the investment in improving the routes
% runlength is the number of trials to simulate
% seed is the index of the substreams to use (integer >= 1)
% other is not used

% RETURNS: fn (total profit from making road improvements), FnVar

%   ********************************************
%   *** Code written by David Eckman         ***
%   ***        dje88@cornell.edu             ***
%   ********************************************

% Note: RandStream.setGlobalStream(stream) can only be used for Matlab
% versions 2011 and later
% For earlier versions, use the method RandStream.setDefaultStream(stream)

FnGrad = NaN;
FnGradCov = NaN;
constraint = NaN;
ConstraintCov = NaN;
ConstraintGrad = NaN;
ConstraintGradCov = NaN;

% Restrict p to lie in the nonnegative orthant
if ((sum(x < 0) >= 1) || (runlength <= 0) || (runlength ~= round(runlength)) || (seed <= 0) || (round(seed) ~= seed))
    fprintf('investments must be nonnegative, runlength should be positive integer, seed must be a positive integer\n');
    fn = NaN;
    FnVar = NaN;

else

    % Generate new streams for arrival process and travel times
    [PoissonProcessStream, TriangleStream, RoutingStream] = RandStream.create('mrg32k3a', 'NumStreams', 3);

    % Set the substream to the "seed"
    PoissonProcessStream.Substream = seed;
    TriangleStream.Substream = seed;
    RoutingStream.Substream = seed;
    
    % Temporarily store old stream
    OldStream = RandStream.setGlobalStream(PoissonProcessStream);
    %OldStream = RandStream.setDefaultStream(PoissonProcessStream); %versions 2010 and earlier

    % Parameters A, R, lambda, TriA, TriB, and TriC are problem-specific

	% Adjacency matrix for network
    A = [0, 1, 1, 1; 1, 0, 1, 1; 1, 1, 0, 1; 1, 1, 1, 0]; % A = ones(nStations,nStations) - eye(nStations) for fully connected network
    [destination, origin] = find(A'); % two vectors containing the pairs of endpoints for routes
    nStations = size(A,1); % Number of stations
    nRoutes = nnz(A); % Number of routes
    routes = find(A); % Indexes of routes in A matrix

    % Routing matrix (including a column for leaving the network)
    R = [0, 0.1, 0.05, 0.05, 0.8; 0.1, 0, 0.05, 0.05, 0.8; 0.05, 0.05, 0, 0.1, 0.8; 0.05, 0.05, 0.1, 0, 0.8];
    
    % Raw arrival rates (by route)
    lambda_mat = [0, 3, 4, 5; 3, 0, 5, 6; 4, 5, 0, 7; 5, 6, 7, 0]./4;
    lambda_mat_trans = lambda_mat';
    lambda_vec = lambda_mat_trans(find(lambda_mat))';

    % Toll fares for each route
    tolls = [0, 1, 1, 1; 1, 0, 1, 1; 1, 1, 0, 1; 1, 1, 1, 0];

    % Investments (as a matrix)
    route_invest = zeros(nStations, nStations);
    route_invest(routes) = x;
    route_invest = route_invest';

    % Make travel times triangular (a=0, b=1, c determined by investments x);
    TriA = zeros(nStations,nStations);
    TriB = ones(nStations,nStations);
    TriC = TriA + (TriB - TriA).*exp(-route_invest);
    
    T = 96; % Length of time horizon for sample path
    
    % Total investment costs
    costs = sum(x);

	% Toll revenues for each simulation of the network
	revenue = zeros(runlength,1);

    for r = 1:runlength
        
        % Set stream for generating passenger arrivals
        RandStream.setGlobalStream(PoissonProcessStream);
        %RandStream.setDefaultStream(PoissonProcessStream);

        % Initialize system state (empty)
        NextArrivalAtDest = Inf*ones(1, nRoutes); % Time a vehicle traveling on route (i,j) reaches j
        NextArrivalToOrigExo = exprnd(1./lambda_vec); % Next exogenous arrival to route (i,j)
        QueueLength = zeros(1, nRoutes); % Number of vehicles on or waiting to travel on route (i,j)

        % Set system clock time
        t = 0;

	    while t <= T
    
            % Identify the next internal and external arrival events and corresponding routes
            [tInternalArrival, RouteInternalArrival] = min(NextArrivalAtDest);
            [tExternalArrival, RouteExternalArrival] = min(NextArrivalToOrigExo);

            % Advance system clock 
            [t, NextEventType] = min([tInternalArrival, tExternalArrival]);

            % Branch on whether the next arrival is internal or external
            if NextEventType == 1 % If the event is an internal arrival...

                O = origin(RouteInternalArrival); % origin
                D = destination(RouteInternalArrival); % destination

                revenue(r) = revenue(r) + tolls(O,D); % Collect toll from completed trip

                % Remove vehicle from queue
                QueueLength(RouteInternalArrival) = QueueLength(RouteInternalArrival) - 1;

                % Set stream for generating passenger arrivals
                RandStream.setGlobalStream(RoutingStream);
                %RandStream.setDefaultStream(RoutingStream);

                % Check if the vehicle will make another trip
                U = rand();
                dest_cumul_dist = cumsum(R(D,:)); % Routing from current destination to new destination
                NewDestination = find(dest_cumul_dist >= U, 1);

                if NewDestination == nStations + 1 % Car leaves network
                    % Do nothing
                elseif NewDestination <= nStations % Car moves elsewhere
                    % Determine index of new route
                    [~, NewRouteID] = max((origin == D).*(destination == NewDestination));
               

                    % Check if there is another vehicle on the new route
                    if QueueLength(NewRouteID) == 0; % If no vehicles, send the vehicle

                        % Set stream for generating travel times
                        RandStream.setGlobalStream(TriangleStream);
                        %RandStream.setDefaultStream(TriangleStream);

                        % Generate new travel time
                        NewD = NewDestination;
                        a = TriA(D,NewD);
                        b = TriB(D,NewD);
                        c = TriC(D,NewD);
                        TravelTime = GenerateTravelTime(D,NewD, a, b, c);
                        
                        % Add arrival at destination to the event list
                        NextArrivalAtDest(NewRouteID) = t + TravelTime; % Update next event time

                    else % Add car to queue
                        % Do nothing
                    end

                    % Add car to queue at new route
                    QueueLength(NewRouteID) = QueueLength(NewRouteID) + 1;

                end

                

                % Check if there is another vehicle to send on the original route
                if QueueLength(RouteInternalArrival) == 0; % If no vehicles waiting...
                    NextArrivalAtDest(RouteInternalArrival) = Inf;

                elseif QueueLength(RouteInternalArrival) > 0 % If at least one vehicle waiting

                    % Set stream for generating travel times
                    RandStream.setGlobalStream(TriangleStream);
                    %RandStream.setDefaultStream(TriangleStream);

                    % Generate new travel time
                    a = TriA(O,D);
                    b = TriB(O,D);
                    c = TriC(O,D);
                    TravelTime = GenerateTravelTime(O, D, a, b, c);

                    % Add arrival at destination to the event list
                    NextArrivalAtDest(RouteInternalArrival) = t + TravelTime; % Update next event time
                end



            elseif NextEventType == 2 % If the event is an external arrival...
                
                % Check if the route is not in use.
                if QueueLength(RouteExternalArrival) == 0; % If route is unused, send vehicle on route

                    % Set stream for generating travel times
                    RandStream.setGlobalStream(TriangleStream);
                    %RandStream.setDefaultStream(TriangleStream);

                    % Generate new travel time
                    O = origin(RouteExternalArrival); % origin
                    D = destination(RouteExternalArrival); % destination
                    a = TriA(O,D);
                    b = TriB(O,D);
                    c = TriC(O,D);
                    TravelTime = GenerateTravelTime(O, D, a, b, c);
                    
                    % Add arrival at destination to the event list
                    NextArrivalAtDest(RouteExternalArrival) = t + TravelTime; % Update next event time
                end

                % Add vehicle to the route
                QueueLength(RouteExternalArrival) = QueueLength(RouteExternalArrival) + 1;

                % Set stream for generating passenger arrivals
                RandStream.setGlobalStream(PoissonProcessStream);
                %RandStream.setDefaultStream(PoissonProcessStream);

                % Generate next external arrival to the route
                NextArrivalToOrigExo(RouteExternalArrival) = t + exprnd(1./lambda_vec(RouteExternalArrival));

            end

        end
        
    end

    %Restore old Random number stream
    RandStream.setGlobalStream(OldStream);
    %RandStream.setDefaultStream(OldStream); %versions 2010 and earlier

    % Record final mean and variance of cumulative costs
	fn = mean(revenue - costs);
	FnVar = var(revenue - costs);

end

function TravelTime =  GenerateTravelTime(O, D, a, b, c);
U = rand();
if U < (c - a)/(b - a);
    TravelTime = a + sqrt(U*(b - a)*(c - a));
else
    TravelTime = b - sqrt((1-U)*(b - a)*(b - c));
end
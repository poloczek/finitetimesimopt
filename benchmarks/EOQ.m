function [fn, FnVar, FnGrad, FnGradCov, constraint, ConstraintCov, ConstraintGrad, ConstraintGradCov] = EOQ(x, runlength, seed, ~)
% function [fn, FnVar, FnGrad, FnGradCov, constraint, ConstraintCov, ConstraintGrad, ConstraintGradCov] = EOQ(x, runlength, seed, other);

%% -- INPUTS:
% x is Q-order quantity
% runlength is the number of replications to simulate
% seed is the index of the substreams to use (integer >= 1)
% other is not used

%% -- OUTPUTS:
% RETURNS fn, FnVar --mean and variance of total cost

%   *************************************************************
%   ***             Written by Danielle Lertola               ***
%   ***         dcl96@cornell.edu    July 23, 2012            ***
%   ***   Parameter Units don't match, needs adjustment 7/23/12**
%   ***            Edited by Jennifer Shih                    ***
%   ***          jls493@cornell.edu    June 20th, 2014        ***
%   *** Edited by Shane Henderson sgh9@cornell.edu, 7/14/14   ***
%   *************************************************************
%
% Last updated Jul 14, 2014
%
%Note: RandStream.setGlobalStream(stream) can only be used for Matlab
%versions 2011 and later
%For earlier versions, use the method RandStream.setDefaultStream(stream)

%% -- SET known outputs 
constraint = NaN;
ConstraintCov = NaN;
ConstraintGrad = NaN;
ConstraintGradCov = NaN;

%% -- CHECK FOR ERRORS; 

if (x < 0) || (runlength <= 0) || (seed <= 0) || (round(seed) ~= seed),
    fprintf('x should be nonnegative, runlength should be positive and real, seed should be a positive integer\n');
    fn = NaN;
    FnVar=NaN; 
    FnGrad=NaN;
    FnGradCov =NaN;
else % main simulation
    
    %% *********************PARAMETERS*********************
    
    mu=1040;    % mean of gamma demand, nominal demand value
    delta=104;  % std dev of gamma demand 
    k=10;       % fixed cost to place order
    c=15;       % variable cost to place order (dollars/ unit-quantity)
    h=2.5;      % holding cost for one unit of inventory/unit of time
    nReps=runlength;    %number of replications

    %% GENERATE RANDOM NUMBER STREAMS
    % Generate new stream for demand rate realizations
    GammaStream= RandStream.create('mrg32k3a', 'NumStreams', 1);
    % Set the substream to the "seed"
    GammaStream.Substream = seed;
    
    %% Generate random data
    OldStream = RandStream.setGlobalStream(GammaStream);     % Temporarily store old stream
    %OldStream = RandStream.setDefaultStream(GammaStream); % for versions 2010 and earlier
    shape=mu^2/delta^2;
    scale=delta^2/mu;
    rate=gamrnd(shape,scale,nReps,1);                         % constant demand rate
    RandStream.setGlobalStream(OldStream);                   % Restore old random number stream
    %RandStream.setDefaultStream(OldStream); %for versions 2010 and earlier
    
    
    %% Main
    MeanRate = mean(rate);
    VarRate = var(rate);
    fn = (c + k / x) * MeanRate + h * x / 2;    %value of objective
    FnVar = (c + k / x)^2 * VarRate / nReps;
    FnGrad = (-k / x^2) * MeanRate + h / 2;
    FnGradCov = (k^2 / x^4) * VarRate / nReps;
end
    
    
    
%   ****************************************
%   ***  Code written by Jennifer Shih   ***
%   ***        jls493 @cornell.edu       ***
%   *** Adapted from an earlier version  ***
%   ***           by Victor Wu           ***
%   ***      Edited by Bryan Chong       ***
%   ***        bhc34@cornell.edu         ***
%   ****************************************

% Last Updated Sept 28, 2014

function [fn, FnVar, FnGrad, FnGradCov, constraint, ConstraintCov, ConstraintGrad, ConstraintGradCov] = ProductionLine(x,runlength, seed, ~)
% function [fn, FnVar, FnGrad, FnGradCov, constraint, ConstraintCov, ConstraintGrad, ConstraintGradCov] = ProductionLine(x,runlength, seed, other);
%
% x is a vector of the service rates (exponentially distributed) (1 x N)
% runlength is the number of replications (each of time length t)
% seed is the index of the substreams to use (integer >= 1)
% other is not used

% lambda is the arrival rate
% N is the number of servers
% K is a vector of current queue length (including part being processed) for each server, 0<=K(i)<=k+1, where k is queue capactiy (1 x N)
% C is a vector of costs to run each server (1 x N)
% t is length of time to run the simulation

% Events List
% Type 1: Arrival (to Q1)
% Type 2: Service completion

%Note: RandStream.setGlobalStream(stream) can only be used for Matlab
%versions 2011 and later
%For earlier versions, use the method RandStream.setDefaultStream(stream)
%

FnGrad = NaN;
FnGradCov = NaN;
constraint = NaN;
ConstraintCov = NaN;
ConstraintGrad = NaN;
ConstraintGradCov = NaN;
if (sum(x < 0) > 0)||(sum(x > 2.00000000001) > 0) || (runlength <= 0) || (runlength ~= round(runlength)) || (seed <= 0) || (round(seed) ~= seed)
    fprintf('x should be between 0 and 2, runlength should be positive integer, seed must be a positive integer\n');
    fn = NaN; 
    FnVar = NaN;
else

%Parameters
lambda = 0.5;
N = 3;  % Number of servers
k = 10; % Capacity of all servers 
C = [1 5 9]; 
t = 1000; % time to which to run system
r = 10000;
c0 = 1;
c1 = 400;
expNumArr = 2*t*lambda; % Expected number of arrivals in the allocated time period, times a safety factor
expNumServiced = ceil(2*t*max(x)); % Expected number of services by the fastest server, times a safety factor

% Generate new streams for part arrivals, service times 
[ArrivalTimeStream, ServerStream] = RandStream.create('mrg32k3a', 'NumStreams', 2);

% Set the substream to the "seed"
ArrivalTimeStream.Substream = seed;
ServerStream.Substream = seed;


% Generate arrival times using designated stream
OldStream = RandStream.setGlobalStream(ArrivalTimeStream); % Temporarily store old stream
%OldStream = RandStream.setDefaultStream(ArrivalTimeStream); %versions 2010 and older

%Generate vector of call arrival times.
tArrivalbig = exprnd(1/lambda, expNumArr, runlength);

%Generate service times for potential customers in time t(may not use all)
sTimebig = zeros(N,expNumServiced,runlength);
RandStream.setGlobalStream(ServerStream);
%RandStream.setDefaultStream(ServerStream); %versions 2010 and earlier

for i = 1:N
    sTimebig(i,:,:) = exprnd(1/x(i), expNumServiced, runlength);
end

% Restore old random number stream
RandStream.setGlobalStream(OldStream);
%RandStream.setDefaultStream(OldStream); %versions 2010 and earlier

revs = zeros(1,runlength); % Revenue per replication
for count = 1:runlength 
    tArrival = tArrivalbig(:,count);
    sTime = sTimebig(:,:,count);
    K = zeros(1,N); % Number of customers in each queue (+1)
    totalFinished = 0; % Total number of completed parts 
    queues = zeros(N,k); % Keep track of customernumbers in each server's queue (first row/server is just zeros)
    firstQ = []; % Queue for first server (since it has infinite capacity) 
    block = zeros(1,N); % Which servers are blocked (0 if not blocked, customer # if blocked)  
    s = ones(1,N); % Counter for keeping track of the random service times used at each server
   
    % First arrival
    K(1) = 1;
    a = 2; % Index of next arrival 
    time = tArrival(1); 
    event = [time + sTime(1,1); 2; 1; 1]; % Events list: [Event time; Event type; Associated server; Customer number];
    s(1) = s(1) + 1; % Increment first server's counter by 1

    % Generate all arrivals up to time t
    temptime = time; 
    while temptime < t
        temptime = temptime + tArrival(a); 
        event = [event [temptime; 1; 1; a]];
        a = a+1;
    end
    
    % Begin Simulation
    [~, nextEvent] = min(event(1,:)); 
    time = event(1,nextEvent);
    type = event(2, nextEvent);
    server = event(3, nextEvent);
    customerNumber = event(4,nextEvent);
 
    while(time < t)
        if type == 1 % Arrival 
            if K(1) == 0 % Server is free 
                % Create service completion event
                event(:,nextEvent) = [time + sTime(1,s(1)); 2; server ; customerNumber];
                s(1) = s(1)+1; 

            else % Server 1 is not free, so add to server 1's queue
                firstQ = [firstQ customerNumber]; % Add this customer to server 1's queue
                event(:,nextEvent) = []; % Remove event from events list

            end 
            K(1) = K(1)+1; % Increment number of customers in server 1's queue

        elseif type == 2 % Service Completion
            if server == N % If server was final server
                K(server) = K(server) - 1;
                totalFinished = totalFinished + 1; 
                event(:,nextEvent) = []; % Remove event from events list
                
            else
                if K(server + 1) == 0 % Next server is free
                    K(server) = K(server) - 1;
                    K(server +1) = 1; 
                    % Create service completion event
                    event(:,nextEvent) = [time + sTime(server + 1, s(server + 1)); 2; server + 1; customerNumber];
                    s(server + 1) = s(server + 1) + 1; 

                
                elseif (K(server+1) <= k) % Next server is not free, but there is room in queue. Note that K includes customer being served.
                    K(server) = K(server) - 1;
                    K(server + 1) = K(server + 1) + 1;
                    queues(server + 1, K(server + 1) - 1) = customerNumber; %add this customer to the queue 
                    event(:,nextEvent) = []; % Remove event from events list

                else % No room in next server's queue - block the server
                    block(server) = customerNumber; 
                    event(:,nextEvent) = []; % Remove event from events list
                    
                end 
            end

            % If not blocked, serve next customer (if there is one)
            if (K(server) > 0 && block(server) == 0)
                if server == 1
                    nextCust = firstQ(1);
                    firstQ(1) = []; % Remove from queue
                    
                else
                    nextCust = queues(server, 1); 
                    queues(server, :) = [queues(server, 2:k) 0]; %remove from queue
                    
                end 
                % Create service completion event
                event = [event [time + sTime(server, s(server)); 2; server; nextCust]] ;
                s(server) = s(server) + 1; % Increment server's RV counter by 1

            end 

            % Check for chains of blocks for previous servers (only if not
            % currently at the first server)
            if (server > 1 && K(server) <= k)
                j = server - 1; % Current server being checked
                go = 1; % Flag for loop
                while go == 1
                    if block(j) > 0 
                        queues(j + 1,k) = block(j); % Add the blocked customer to the end of next queue 

                        if K(j) > 1 % There are customers in queue, behind blocked customer
                            nextCust = queues(j, 1);
                            % Create service completion event
                            event = [event [time + sTime(j, s(j)); 2; j; nextCust]] ;
                            s(j) = s(j) + 1;
                        end
                        
                        block(j) = 0; % Unblock previously blocked server
                        K(j + 1) = K(j + 1) + 1; 
                        K(j) = K(j) - 1; 
                        j = j - 1; % Continue checking previous servers to see if they are blocked
                        if j == 0 % Finished checking all servers 
                            go = 0; 
                        end
                    else % End of blocked chain 
                        go = 0; 
                    end
                end
            end

        end %end of types if
        
        % Move clock forward 
        [~, nextEvent] = min(event(1,:));
        time = event(1,nextEvent);
        type = event(2, nextEvent);
        server = event(3, nextEvent);
        customerNumber = event(4,nextEvent);
    end %end of while loop

    revs(count) = r*(totalFinished/time)/(c0+C*x')-c1;
end 
fn = mean(revs);
FnVar = var(revs)/runlength; 
end
end

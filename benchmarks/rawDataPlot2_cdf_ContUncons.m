function rawDataPlot2_cdf_ContUncons()
% By Anna Dong
% Plot on continuous, unconstrained problems.
AlgsNames = {'Alg_GradSearch','Alg_GradSearchRS','Alg_NelderMead','Alg_RandomSearchI'};
AlgsNamesLegend = {'GradSearch','GradSearchRS','NelderMead','RandomSearchI'};
% ProblemNames = {'SAN','QueueGG1','EOQ','DualSourcing','Rosenbrock','MM1',...
%     'MaintenanceOptimization','Ambulance','MultiModal',...
%     'CtsNews','FacilityLocation','ParameterEstimation','ProductionLine'};
ProblemNames = {'Ambulance','CtsNews','DualSourcing','EOQ','FacilityLocation',...
    'MaintenanceOptimization','MM1','ParameterEstimation','QueueGG1','Rosenbrock'};
% %Testing, to be removed
%ProblemNames = {'CtsNews'};
%plotseed = 99;
AlgsColors = {'m','b','r','g'};
minmaxList = {'minimize','nah','maximize'};


numAlgs = length(AlgsNames);
numProblems = length(ProblemNames);
% Compute e(i,j) for algorithm i & problem j, transfer all to minimization
eijM = zeros(numAlgs,numProblems);
ebadV = zeros(1,numProblems);
for j=1:numProblems
    problemname = ProblemNames{j};
    problemstructhandle=str2func(strcat(problemname, 'Structure'));
    [minmax, ~, ~, ~, ~, ~, ~, ~, budget, ~, ~] = problemstructhandle(1, 19);
    ebadTemp = zeros(numAlgs,1);
    for i=1:numAlgs
        fnmean = csvread(strcat(pwd,'/_RawData/RawData_',AlgsNames{i},'_on_',problemname,'_mean.csv'));
        eijM(i,j) = -minmax*sum(fnmean); % transfer to minimization problem
        ebadTemp(i) = max(-minmax*fnmean); % transfer to minimization problem
    end
    ebadV(j) = max(ebadTemp)*length(budget); % transfer to minimization problem
end
%find estar(j) & ebad(j)
estarV = min(eijM);
% ebadV = zeros(1,numProblems); %initial value are worst values
% for j2=1:numProblems
%     [~, ~, ~, ~, ~, ~, ~, ~, budget, ~, ~] = problemstructhandle(1, 19);
%     fnmean0 = csvread(strcat(pwd,'/_RawData/RawData_init','_on_',ProblemNames{j2},'_mean.csv'));
%     ebadV(j2) = -minmax*fnmean0*length(budget); %or use eLowerStar
% end

%compute r(i,j)
rijM = zeros(numAlgs,numProblems);
%ebadV = ebadV + 0.1; % avoid numerical errors
for i3=1:numAlgs
    for j3=1:numProblems
        rijM(i3,j3) = (eijM(i3,j3)-estarV(j3)) / (ebadV(j3)-estarV(j3))
    end
end
%% plot cdf of r(i,j from 1 to #problems)
upStep = 1/numProblems;
y = 0:upStep:1;
%xM = cumsum(rijM,2); %by row
xM = cumsum(sort(rijM,2),2); %by row
xmax = max(xM(:,end));
%rng(plotseed);
for i=1:numAlgs
    plot([0,xM(i,:),xmax],[y,1],'-o', 'Color', AlgsColors{i});
    hold on
end
legend(AlgsNamesLegend);
xlabel('r(i,j)','FontSize',12); ylabel('j (Problems)','FontSize',12);
title(['Comparison of Algorithms cdf (',num2str(numProblems),' problems)'],'FontSize',12);
axis([-0.5,max(xM(:,end))*1.25, 0, 1.1]);




end
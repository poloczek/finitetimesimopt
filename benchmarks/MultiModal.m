
%   ***************************************
%   ***    Code written by Lewis King   ***
%   ***         lgk36@cornell.edu       ***
%   ***    Edited by Jennifer Shih      ***
%   ***        jls493@cornell.edu       ***
%   ***    Edited by Bryan Chong        ***
%   ***        bhc34@cornell.edu        ***
%   ***************************************
%
%   Last updated October 2, 2014

%RETURNS: the optimal value of g1 as described in problem statement
%f2=sin(.05*pi*x)^6/(2^(2*((x-10)/80)^2));

function [fn, FnVar, FnGrad, FnGradCov, constraint, ConstraintCov, ConstraintGrad, ConstraintGradCov] = MultiModal(x, runlength, seed, ~)
% function [fn, FnVar, FnGrad, FnGradCov, constraint, ConstraintCov, ConstraintGrad, ConstraintGradCov] = MultiModal(x, runlength, seed, other);
% x is the vector (x_1, x_2)
% runlength is the number of replications
% seed is the index of the substreams to use (integer >= 1)
% other is not used

constraint = NaN;
ConstraintCov = NaN;
ConstraintGrad = NaN;
ConstraintGradCov = NaN;

if (sum(x < 0) > 0) || (runlength <= 0) || (runlength ~= round(runlength)) || (seed <= 0) || (round(seed) ~= seed),
    fprintf('x should be >= 0, runlength should be positive integer, seed must be a positive integer\n');
    fn = NaN;
    FnVar = NaN;
    FnGrad = NaN;
    FnGradCov = NaN;
else
    
    %x_opt_known=[10,10];
    nReps=runlength;
    
    % Generate a new stream for random numbers
    NormStream = RandStream.create('mrg32k3a');
    
    % Set the substream to the "seed"
    NormStream.Substream = seed;
    OldStream = RandStream.setGlobalStream(NormStream);
    %OldStream = RandStream.setDefaultStream(NormStream); % Version 2011 and earlier
    sigma = 0.3^2 * nReps;
    Norm = normrnd(0,sqrt(sigma));
    
    RandStream.setGlobalStream(OldStream); %Return to old stream
    %RandStream.setDefaultStream(OldStream); %Version 2011 and earlier
    
    
    f1 = sin(.05*pi*x(1))^6/(2^(2*((x(1)-10)/80)^2));
    f2 = sin(.05*pi*x(2))^6/(2^(2*((x(2)-10)/80)^2));
    
    fn=-(f1 + f2) + Norm/nReps;
    FnVar=0.3^2 * nReps;
    FnGrad = NaN;
    FnGradCov = NaN;
end
end

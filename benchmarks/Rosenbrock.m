
%   ***************************************
%   *** Code written by German Gutierrez***
%   ***         gg92@cornell.edu        ***
%   ***                                 ***
%   *** Updated by Shane Henderson to   ***
%   *** use standard calling and random ***
%   *** number streams                  ***
%   ***     Edited  by Bryan Chong      ***
%   ***         bhc34@cornell.edu        ***
%   ***************************************

% Last updated Sept 17, 2014

%RETURNS: Minimum value of G_m encountered throughout the simulation.
%Initially set to 10,000 replications.


function [fn, FnVar, FnGrad, FnGradCov, constraint, ConstraintCov, ConstraintGrad, ConstraintGradCov] = Rosenbrock(x, runlength, seed, other)
%function [fn, FnVar, FnGrad, FnGradCov, constraint, ConstraintCov,ConstraintGrad, ConstraintGradCov] = Rosenbrock(x, runlength, seed, other);
% x is vector of length 2*q
% runlength is the number of trials to simulate
% seed is the index of the substreams to use (integer >= 1)
% other represents q,
%
%Note: RandStream.setGlobalStream(stream) can only be used for Matlab
%versions 2011 and later
%For earlier versions, use the method RandStream.setDefaultStream(stream)
%

constraint = NaN;
ConstraintCov = NaN;
ConstraintGrad = NaN;
ConstraintGradCov = NaN;
thres = 0.0000005;
if (sum(x > 10+thres) > 0) ||(sum(x < -10-thres) > 0) || mod(length(x),2) ~= 0 || (runlength <= 0) || (runlength ~= round(runlength)) || (seed <= 0) || (round(seed) ~= seed)
    fprintf('x should be within [-10,10] and of even length, runlength should be positive integer, seed must be a positive integer\n');
    fn = NaN;
    FnVar = NaN;
    FnGrad = NaN;
    FnGradCov = NaN;
    
else
    
    q=length(x)/2;
    m=500; %number of times to sample at location x to find Gm(x)
    nTrials=runlength;
    G_m=zeros(nTrials,1);
    
    % Generate a new stream for random numbers
    OurStream1 = RandStream.create('mrg32k3a', 'NumStreams', 2);
    
    % Set the substream to the "seed"
    OurStream1.Substream = seed;
    
    OldStream = RandStream.setGlobalStream(OurStream1);
    %Generate error terms for G_m(x)
    RandStream.setGlobalStream(OurStream1)
    %RandStream.setDefaultStream(OurStream1)
    norm=normrnd(0,1,1,nTrials);
    
    RandStream.setGlobalStream(OldStream); %Restore old stream
    %RandStream.setDefaultStream(OldStream);
    
    g=0;
    for i=1:2:2*q-1
        g=g+((1-x(i))^2)+(100*((x(i+1)-(x(i)^2))^2));
    end
    
    for i=1:nTrials
        G_m(i) = g+sqrt((1+g)/m)*norm(i);
    end
    
    fn=mean(G_m);
    FnVar = var(G_m)/nTrials;
    FnGrad = NaN;
    FnGradCov = NaN;
end
end
